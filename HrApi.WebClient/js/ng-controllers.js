﻿app.controller('ImpersonationController', function ($rootScope, $scope, $http, $location, $timeout, fetch) {
    $scope.vm = fetch.data;
    $scope.ImpersonatingUser = {};
    for (var i in $scope.vm.Users) { // array, convert to hash:
        var u = $scope.vm.Users[i];
        if (u.Value == $scope.vm.ImpersonatingUserId) {
            $scope.ImpersonatingUser = u;
        }
    }

    $scope.changeUser = function () {
        var posturl = apiRoot + '/Impersonation';
        try {
            $('#Message').html('updating impersonation...');
            $http({
                method: 'PUT',
                url: posturl,
                data: { Id: $scope.ImpersonatingUser.Value },
                withCredentials: true
            })
            .success(function (data) {
                $rootScope.user = data;
                $timeout(function () {
                    $('#Message').html('');
                }, 300);
            })
            .error(function (data, status, errors, config) {
                alert('ERROR:\n' + data.Message);
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    }
})
.controller('ParListController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    $scope.vm = fetch.data;
    $scope.vm.Query.FromDate = $filter('date')($scope.vm.Query.FromDate, 'M/d/yy');
    $scope.vm.Query.ToDate = $filter('date')($scope.vm.Query.ToDate, 'M/d/yy');
    if ($scope.vm.Query.ActionCode) {
        // TODO: hash, cache vm ui components (e.g. selectors), only iterate here if uncached...
        for (var i in $scope.vm.ActionTypes) { // array, convert to hash:
            var c = $scope.vm.ActionTypes[i];
            if (c.Value == $scope.vm.Query.ActionCode) {
                $scope.SelectedActionType = c;
            }
        }
    }
    if ($scope.vm.Query.WorkflowStatusId) {
        for (var i in $scope.vm.ApprovalStatuses) { // array, convert to hash:
            var s = $scope.vm.ApprovalStatuses[i];
            if (s.Value == $scope.vm.Query.WorkflowStatusId) {
                $scope.SelectedApprovalStatus = s;
            }
        }
    }
    $scope.$on('$locationChangeSuccess', function (event) {
        // *NOTE*, path check fails unless wrapped in timeout:
        $timeout(function () {
            var p = $location.path();
            if (!p || p.indexOf('/par' != 0)) {
                return false;
            }
            $('#Message').html('searching PARs...');
        });
        var q = $location.search();
        var qs = [];
        for (var k in q) {
            qs.push(k + '=' + q[k]);
        }
//        console.log('selected action:', $scope.SelectedAction);
        try {
            $http({ // promise
                method: 'GET',
                url: apiRoot + '/par' + (qs.length ? '?' + qs.join('&') : ''),
                withCredentials: true
            }).success(function (d) {
                // moved hide() to grid.dataBound.
                //$timeout(function () {
                //    $scope.asyncNotification.hide();
                //}, 200);
                $scope.vm = d;
//                for (var i in $scope.vm.ActionTypes) { // array, convert to hash:
//                    var c = $scope.vm.ActionTypes[i];
//                    //$scope.ActionCodes[c.Value] = c.Text;
////                    console.log(c.Value, $scope.vm.Query.ActionCode);
//                    if (c.Value == $scope.vm.Query.ActionCode) {
//                        console.log('matched ' + c.Value);
//                        $scope.SelectedActionType = c;
//                    }
//                }
//                 not really using this link, using data() method now instead.
                // http://stackoverflow.com/questions/18399805/reloading-refreshing-kendo-grid
                // data() wraps Results array in kendo.data.ObservableObject...
                $scope.parGrid.dataSource.data(d.Results);
        //        $timeout(function () {
        //            if (d.Query.ActionCode) {
        //                for (var i in $scope.vm.ActionTypes) { // array, convert to hash:
        //                    var c = $scope.vm.ActionTypes[i];
        //                    //$scope.ActionCodes[c.Value] = c.Text;
        ////                    console.log(c.Value, $scope.vm.Query.ActionCode);
        //                    if (c.Value == d.Query.ActionCode) {
        //                        console.log('matched ' + c.Value);
        //                        $scope.SelectedActionType = c;
        //                    }
        //                }
        //            }
        //        });
//                http://www.telerik.com/forums/howto-reset-virtual-scrolling-on-datasource-query
                //$timeout(function () {
                //    if (d.Query.UpdatedSince) {
                //        //                    d.Query.UpdatedSince = parseDate(d.Query.UpdatedSince);
                //        d.Query.UpdatedSince = kendo.parseDate(d.Query.UpdatedSince);
                //    }
                //});
            })
            .error(function (data, status, errors, config) {
                //$scope.asyncNotification.hide();
                alert('http error.');
                //            if (data.error) { alert('ERROR: ' + data.error); }
            });
            //});
        }
        catch (xcp) {
            alert('xcp:' + JSON.stringify(xcp));
        }
    });

    //if (vm.Query.ActionCode) {
    //    vm.SelectedAction = // find item by ActionCode value...
    //}
    $scope.runQuery = function () {
//        console.log('par query', $scope.vm.Query);
        // create query string from query object...
        //$scope.vm.Query.ActionCode = $scope.SelectedActionType.Value;
        $timeout(function () {
            $('#parGrid .k-scrollbar-vertical').scrollTop(0); // need to scrollTop *BEFORE* updating data source...looks like bug in Grid virtual scrolling
        });
        var q = $scope.vm.Query;

        var qp = []; // Query Params
        if ($scope.SelectedActionType) {
            qp.push('ActionCode=' + $scope.SelectedActionType.Value);
        }
        if ($scope.SelectedApprovalStatus) {
            qp.push('WorkflowStatusId=' + $scope.SelectedApprovalStatus.Value);
        }

        if (typeof q.FromDate == 'object') { // Date, not string. (first submit goes as string.)
            q.FromDate = $filter('date')(q.FromDate, 'M/d/yy');
        }
        if (q.FromDate) qp.push('FromDate=' + q.FromDate);
        if (q.ToDate) qp.push('ToDate=' + q.ToDate);
        if (q.Query) qp.push('Query=' + escape(q.Query));

        // TODO: handle when '/pd?[query string]'...
        if (qp.length) {
            $location.search(qp.join('&')); // reloadOnSearch = false.
        }
        else {
            $location.search('');
        }
    }
    $scope.options = {
        Grid: {
            columns: [
                {
                    field: "Id",
                    template: function (d) {
                        return "<a href='#/par/" + d.Id + "'>" + d.Id + "</a>"
                    },
                    title: "PAR#",
                    width: "25px"
                },
                {
                    field: "EffectiveDate",
                    template: function (d) {
                        return d.EffectiveDate ? $filter('date')(kendo.parseDate(d.EffectiveDate), 'M/d/yy') : '';
                    },
                    title: "Effective",
                    width: "35px"
                },
                {
                    field: "Originator",
                    title: "Originator",
                    width: "50px"
                },
                {
                    field: "ActionDescription",
                    title: "Action Type",
                    width: "60px"
                },
                {
                    field: "ActionType",
                    title: "Reason/Contract",
                    width: "70px"
                },
                {
                    field: "PositionId",
                    title: "Pos#",
                    template: function (d) {
                        return '<a href="#/position/' + d.PositionId + '">' + d.PositionId + '</a>';
                    },
                    width: "25px"
                },
                {
                    field: "EmpLastFirst",
                    title: "Employee",
                    template: function (d) {
                        return d.EmpLastFirst ? d.EmpLastFirst : '';
                    },
                    width: "60px"
                },
                {
                    field: "OrgUnitShortName",
                    title: "Team",
                    width: "24px"
                },
                {
                    field: "ApprovalStatusDesc",
                    title: "Stage",
                    width: "60px"
                }
            ],
            dataSource: {
                data: $scope.vm.Results,
                pageSize: 18
            },
            dataBound: function (e) {
                $timeout(function () {
                    $('#Message').html('');
                }, 200);
            },
            height: 550,
            //pageable: true,
            //rebind: $scope.vm.Results, doesn't seem to work...
            resizable: true,
            scrollable: {
                virtual: true
            },
            schema: {
                model: {
                    id: "Id"
                }
                // see schema.parse for custom pre-parsing of vm
            },
            sortable: true
        }
    };
})
.controller('ParController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    $timeout(function () {
        $('#Message').html('');
    }, 200); // hide "rendering..."
    // config options for KendoUI widgets:
    $scope.vm = fetch.data;
    // TODO: auto-parse + filter all Date types from API...
    //$filter('date')(kendo.parseDate(d.EndDate), 'M/d/yy')
    $scope.vm.Par.EffectiveDate = $filter('date')(kendo.parseDate($scope.vm.Par.EffectiveDate), 'M/d/yy');
    $scope.options = {
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            //expandMode: "multiple"
            expandMode: "single",
        }
    }
})
.controller('PdController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    $('#Message').html('rendering...');
    //$scope.asyncNotification.show({ message: 'rendering Position Description...' }, 'async-op'); // info, success, warning, error...
    $scope.vm = fetch.data; // *NOTE*, using separate PdListController.
    // unfortunately seems to be no way to bind by value in ng-option???
    $scope.JobClasses = {};
    for (var i in $scope.vm.JobClasses) { // array, convert to hash:
        var c = $scope.vm.JobClasses[i];
        $scope.JobClasses[c.Value] = c.Text;
        if (c.Value == $scope.vm.Description.ProposedJobId) {
            $scope.ProposedJobClass = c;
        }
        if (c.Value == $scope.vm.Description.TopJobClassSupervised) {
            $scope.TopJobClass = c;
        }
    }
    // TODO: Angular directive to set CurrentJobName via JobClasses...???
    // this all comes from using EF to pull PD directly from table.
    // other option is additional server-side lookup.
    // TODO: create SQL Views for simple read-only joins like this:
    // global HACK!!!:  TODO: pull JobClasses from API, not MVC-embedded
    //var jobclasses = pdfDetailVm.JobClasses; //
    //$scope.currentJobName = '';
    //for (var c in jobclasses) {
    //    if (jobclasses[c].Value && jobclasses[c].Value == $scope.vm.Description.xCurrentJobId) {
    //        $scope.currentJobName = jobclasses[c].Text;
    //        break;
    //    }
    //}
    //document.title = "PDF #" + $scope.vm.Description.Id + " Detail";
    //alert('vm: ' + JSON.stringify($scope.vm.Duties));
    //    $scope.$routeParams = $routeParams; // pdfId is passed in $routeParams...

    // TODO: look up Pdf Detail from $routeParams.pdfId... for now just copying from parent scope.
    //alert('root scope working title: ' + $scope.$parent.vm.Pos.WorkingTitle);

    // TODO: this is stupid little demo...replace with actual Pdf detail lookup in RESOLVE of route.
    //var pdfs = $scope.$parent.vm.Pdfs;
    //for (var i = 0; i < pdfs.length; i++) {
    //    //if (pdfs[i].Id == $routeParams.pdfId) {
    //    if (pdfs[i].Id == id) {
    //        $scope.vm = pdfs[i]; // set vm to matching pdf record.
    //    }
    //}

    //alert('dvm:' + JSON.stringify(pdfDetailVm));
    $scope.options = {
        PositionType: {
            dataSource: [
                { Id: "C", Name: "Classified" },
                { Id: "X", Name: "Exempt" },
                { Id: "W", Name: "WMS" }
            ],
            dataValueField: "Id",
            dataTextField: "Name",
            optionLabel: " - select - "
        },
        //JobClass: {
        //    //dataSource: pdfDetailVm.JobClasses,
        //    dataSource: $scope.vm.JobClasses,
        //    dataValueField: "Value",
        //    dataTextField: "Text",
        //    optionLabel: " - select - ",
        //    template: '<span class="options">#: Text#</span>'
        //},
    // *note*, these are options for Kendo-UI widgets, not <option>s:
    //    // also note, DutyEditor should be data-bound to CurrentlyEditingDuty or similar in VM.
        DutyEditor: {
            encoded: false, // else Angular double-encodes
            tools: [
                "bold", "italic", "underline",
                "insertUnorderedList",
                "createLink", "unlink",
                // now auto-saving on change event (fires only when content has changed and editor is blurred);
                // see SiteBasic.css, .k-save for example of setting icon from sprite.
                // but this is for saving edits to title, percent asst. (could also be done on blur).
                {
                    name: "save",
                    tooltip: "save changes",
                    exec: function (e) { // e is button click event...not sure whether we need it...
                        // *YIKES*, only apparent way to trigger sync with Angular model (change event in editor IFRAME)
                        // www.telerik.com/forums/editor-change-event
                        $($(this).data('kendoEditor').window).blur();
                        // this is only necessary for saving title/percentage if text is unchanged:
//                        $scope.saveDuty();
                    }
                    // templates can also be specified with variable substitution as text/x-kendo-template...
                    //template: "<button class='k-button'><span class='k-icon k-update'>&nbsp;</span>Save</button>"
                }
            ],
            // trying to fix bug where initially editing newitem, then selecting existing item disables editor:
            // seems to work, even though it is theoretically re-focusing an item that just lost focus.
            change: function () {
                $scope.saveDuty();
                //this.focus();
            }
        },
        // TODO: handle focus event of editor IFRAME window, auto-select containing panel if unselected multi-expand.
        DutyPanelBar: {
            animation: {
                collapse: {
                    duration: 300
                    //effects: 'fadeOut'
                },
                expand: {
                    duration: 300,
                    //                effects: 'expandVertical fadeIn'
                    effects: 'expandVertical'
                }
            },
            collapse: function (pb) {
                // exit edit mode on collapse...
                if ($scope.editingDutyId == $(pb.item).attr('id')) {
                    $scope.$apply(function () {
                        $scope.editingDutyId = 0;
                    });
                }
            },
            expand: function (pb) {
                $scope.$apply(function () {
                    $scope.editingDutyId = $(pb.item).attr('id');
                });
            },
            expandMode: "single", // default to single-expand mode.
//            expandMode: "multiple",
            //activate: function () {
            //    $scope.hideTasks(); // hide any floating duty task "tooltips"
            //},
            select: function (pb) {
                $timeout(function () {
                    $scope.editingDutyId = $(pb.item).attr('id');
                    $scope.selectedDutyId = $(pb.item).attr('id');
                    $scope.selectedDutyIndex = $(pb.item).index();
                });
            }
        },
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            expand: function (e) {
                // attaching to expand event as convenient handle for hiding async note for data fetch + render
                // first panel expands by default after rendering...
                $('#Message').html('');
                //$scope.asyncNotification.hide();
//                $('#AsyncMessage').hide(); // yes, it's DOM manipulation in the controller.  mea culpa!
//                $('#PdForm').css('display', 'block');
            },
            expandMode: "single"
        },
        PdEditor: { // save button calls savePD(), not saveDuty().  *NOTE*, removed ng-change event handler trigger that was also calling savePD().
            encoded: false, // else Angular double-encodes
            tools: [
                "bold", "italic", "underline",
                "insertUnorderedList",
                "createLink", "unlink",
                // see SiteBasic.css, .k-save for example of setting icon from sprite.
                {
                    name: "save",
                    tooltip: "save changes",
                    exec: function (e) {
                        // e is button click event...not sure whether we need it...
                        // *YIKES*, only apparent way to trigger sync with Angular model (change event in editor IFRAME)
                        $($(this).data('kendoEditor').window).blur();
                        $scope.savePD();
                    }
                    // templates can also be specified with variable substitution as text/x-kendo-template...
                    //template: "<button class='k-button'><span class='k-icon k-update'>&nbsp;</span>Save</button>"
                }
            ],
            // this.focus() trying to fix bug where initially editing newitem, then selecting existing item disables editor:
            // seems to work, even though it is theoretically re-focusing an item that just lost focus.
            // (apparently fixed in latest Kendo)
            // *NOTE*, change is triggered by blur event in save button exec (see PdEditor.tools config above)
//            change: function () {
//                this.focus();
                //$scope.savePD();
//            }
        }
    };

    $scope.asPdf = function () {
        var pdForm = {
            C: 'WGS',
            W: 'WMS',
            X: 'Exempt'
        };
        // TODO: configurable SSRS server
        var pdfloc = 'http://test-reporting.cts.wa.gov/reportserver?/Human Resources/Print/' + pdForm[$scope.vm.Description.PositionTypeCXW] + '&rs:Command=Render&rs:Format=pdf&Id=' + $scope.vm.Description.Id + '&rs:ClearSession=true';
        //alert('pdfloc:' + pdfloc);
        window.location = pdfloc;
    };

    $scope.collapseAll = function () {
        $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse('li');
    };

    $scope.collapseDuties = function () {
        $scope.dutiesPanelBar.options.expandMode = "single";
        $scope.dutiesPanelBar.collapse('li');
    };

    $scope.collapseQuals = function () {
        $scope.qualsPanelBar.options.expandMode = "single";
        $scope.qualsPanelBar.collapse('li');
    };

    $scope.collapseWx = function () {
        $scope.wxPanelBar.options.expandMode = "single";
        $scope.wxPanelBar.collapse('li');
    };

    $scope.createDuty = function () {
        var newDuty = {
            PositionDescriptionId: $scope.vm.Description.Id,
            Duty: 'New Duty',
            TasksHtml: '<ul><li>Sample Task 1</li><li>Sample Task 2</li></ul>',
            TimePercent: 0
        };

        var posturl = apiRoot + '/PositionDuty/';
        $('#Message').html('Creating Duty...');
//        $scope.asyncNotification.show({ message: 'Creating Duty...' }, 'async-op'); // info, success, warning, error...
        try {
            $http({
                method: 'POST',
                url: posturl,
                data: newDuty,
                withCredentials: true
            })
            .success(function (data) {
                // $timeout should automatically call $apply:
                $timeout(function () {
                    $('#Message').html('');
                    //$scope.asyncNotification.hide();
                    $scope.vm.Duties.push(data);
                }, 200);
            })
            .error(function (data, status, errors, config) {
                $('#Message').html('');
//                $scope.asyncNotification.hide();
                alert('ERROR:\n' + data.Message);
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    $scope.createPar = function () {
        var pd = $scope.vm.Description;
        //alert('Creating CreatePosition PAR for PD# ' + $scope.vm.Description.Id);
        // TODO: test that parData is not null and is an object...
//        var posturl = $scope.appRoot + 'api/par';
        var posturl = $scope.apiRoot + '/par';
        var parData = {
            ActionCode: pd.PositionId ? 'P1' : 'P0', // Update or Create Position...
            PosWorkingTitle: pd.WorkingTitle,
            PositionId: pd.PositionId, // undefined for CreatePosition PARs...
//            PosJobId: pd.ProposedJobId,
            PosJobId: pd.ProposedJobClass ? pd.ProposedJobClass.Value : undefined,
            PosPersonnelSubArea: (pd.PositionTypeCXW == 'W' ? '0002' :
                (pd.PositionTypeCXW == 'X' ? '0003' : null)), // Classified PSA is indeterminate, so null here.
            PosIsSupervisor: pd.IsSupervisor,
            PosPointValue: pd.xProposedJvacEvalPts,
            PdfId: pd.Id
        };
//        $timeout(function () {
            $('#Message').html('Creating PAR...');
//            $scope.asyncNotification.show({ message: 'Creating PAR...' }, 'async-op'); // info, success, warning, error...
//        }); // *this* timeout is to ensure Angular $apply.  Next one is for delay to ensure visibility before redirect.
        $timeout(function () {
            try {
                $http({
                    method: 'POST',
                    url: posturl,
                    data: parData,
                    withCredentials: true
                })
                .success(function (d) {
                    $('#Message').html('');
//                    $scope.asyncNotification.hide();
                    // TODO: set Pd link automatically via extra param to ParController...
//                    $scope.vm.Description.LinkedParId = d.Par.Id
                    $scope.vm.Description.LinkedParId = d.Id;
                    //                    alert('created Par # ' + d.Par.Id);
                })
                .error(function (d, status, errors, config) {
                    $scope.asyncNotification.hide();
                    alert('ERROR\n' + d.Status || d.error);
                });
            }
            catch (xcp) {
                var err = xcp;
                alert('ERROR: ' + JSON.stringify(xcp));
            }
        }, 300);
    }

    // this passes the index of the clicked item, more reliable than selectedDutyIndex...
    $scope.deleteDuty = function (index, evt) {
        var d = $scope.vm.Duties[index];
        evt.stopPropagation(); // prevent toggling expand/collapse...
        if (confirm('this will delete duty #' + d.Id + '\n(' + d.Duty + ')')) {
            $('#Message').html('deleting duty #' + d.Id);
  //          $scope.asyncNotification.show({ message: 'Deleting Duty...' }, 'async-op'); // info, success, warning, error...
            //            var posturl = appRoot + 'api/PositionDuty/' + d.Id;
            var posturl = apiRoot + '/PositionDuty/' + d.Id;
            try {
                $http({
                    method: 'DELETE',
                    url: posturl,
                    withCredentials: true
                })
                .success(function (deleted) {
                    $timeout(function () {
                        $('#Message').html('');
                        //                        $scope.asyncNotification.hide();
                        $scope.vm.Duties.splice(index, 1);
                    }, 300); // 300ms timeout to ensure deletion is visible...
                })
                .error(function (data, status, errors, config) {
                    $('#Message').html('');
//                    $scope.asyncNotification.hide();
                    alert('ERROR: ' + data.Message);
                });
            }
            catch (xcp) {
                alert('$http delete failed.');
            }
        }
    };

    $scope.deletePD = function () {
        var posturl = apiRoot + '/PositionDescription/' + $scope.vm.Description.Id;
        // TODO: allow deleting only PDs in "created" approval state...
        if (!confirm('This will permanently delete Position Description #' + $scope.vm.Description.Id
            + ', along with any associated Duty allocations.  Upon success, you will be redirected to the list of Position Descriptions in your perspective.')) { return false; };
        // TODO: add Kendo Notification of async op status
        $('#Message').html('deleting PDF #' + $scope.vm.Description.Id);
//        $scope.asyncNotification.show({ message: 'Deleting PD...' }, 'async-op'); // info, success, warning, error...
        $http({
            method: 'DELETE',
            url: posturl,
            withCredentials: true
        })
        .success(function (deleted) {
            // this seems to succeed silently for IE...
            $timeout(function () {
                $('#Message').html('');
//                $scope.asyncNotification.hide();
                //                alert("Deleted PD #" + $scope.vm.Description.Id + ".");
                // redirect to PD list for position, use Angular $location to keep routing happy:
                $location.path('/pd');
                //                $location.path('/position/' + $scope.vm.Description.PositionId + '/pd');
            }, 300); // timeout to ensure deletion is visible...
        })
        .error(function (data, status, errors, config) {
            $('#Message').html('');
//            $scope.asyncNotification.hide();
            alert('Error deleting PDF...');
        });
    };

    $scope.dutyCheckKey = function ($event) {
        if ($event.which == 13) { // pressed return
            $scope.editingDutyId = 0;
            $scope.saveDuty();
        }
    };

    $scope.dutyPctSum = function () {
        duties = $scope.vm.Duties;
        var sum = 5; // 5% reserved for "Other"
        for (var i = 0; i < duties.length; i++) {
            if (duties[i].TimePercent > 0) {
                sum += new Number(duties[i].TimePercent);
            }
        }
        return sum;
    };

    //$scope.editDuty = function (index, evt) {
    //    $scope.editingDuty = $scope.vm.Duties[index];
    //    if (evt.stopPropagation) {
    //        evt.stopPropagation();
    //    }
    //    else {
    //        evt.stopImmediatePropagation();
    //    }
    //};

    $scope.editingDutyId = 0;
    ////= null;

    $scope.expandAll = function () {
        // *NOTE, panelbar gets replaced on re-render, thus caching as separate property here.
        $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand('li');
    };

    $scope.expandDuties = function () {
        $scope.dutiesPanelBar.options.expandMode = "multiple";
        $scope.dutiesPanelBar.expand('li');
    };

    $scope.expandQuals = function () {
        $scope.qualsPanelBar.options.expandMode = "multiple";
        $scope.qualsPanelBar.expand('li');
    };

    $scope.expandWx = function () {
        $scope.wxPanelBar.options.expandMode = "multiple";
        $scope.wxPanelBar.expand('li');
    };

    //$scope.hideTasks = function () {
    //    $('#DutyTasksPreview').hide();
    //};

    //$scope.isDutySelected = function (item) {
    //    return ($scope.selectedDutyId && $scope.selectedDutyId == item.Id);
    //};

    $scope.pctkeydown = function ($event) {
        // allow only digits (48-57, 96-105 keypad), backspace (8), delete(46), cursor left (37), right (39), tab (9), enter(13)
        if (!(
            ($event.which > 47 && $event.which < 58)
            || ($event.which > 95 && $event.which < 106)
            || $event.which == 8
            || $event.which == 9
            || $event.which == 13
            || $event.which == 37
            || $event.which == 39
            || $event.which == 46)
            || ($event.shiftKey && $event.which != 9) // allow shift+tab nav
            || $event.ctrlKey
            || $event.metaKey
            ) {
            $event.preventDefault();
        }
    };

    $scope.saveDuty = function () {
        // *NOTE*, with multi-expand accordion, save button may be clicked in non-selected panel...
        // TODO: handle that better, perhaps cause tool buttons to auto-select parent panel?
        // alternatively, could save changes to all Duties at once?...
//        console.log($scope.vm.Duties[$scope.selectedDutyIndex]);
        var d = $scope.vm.Duties[$scope.selectedDutyIndex];
        var posturl = apiRoot + '/PositionDuty/' + d.Id;
        try {
            $('#Message').html('Saving Duty...');
            //            $scope.asyncNotification.show({ message: 'Saving Duty...' }, 'async-op'); // info, success, warning, error...
            $timeout(function () {
                $http({
                    method: 'PUT',
                    url: posturl,
                    data: d,
                    withCredentials: true
                })
                .success(function (data) {
                    $timeout(function () {
                        //                    $scope.asyncNotification.hide();
                        $('#Message').html('');
                    }, 300);
                })
                .error(function (data, status, errors, config) {
                    $('#Message').html('');
                    //$scope.asyncNotification.hide();
                    alert('ERROR:\n' + data.Message);
                    //if (data.error) { alert('ERROR: ' + data.error); };
                    //$scope.asyncOp = false;
                });
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    $scope.savePD = function (cb) {
        // *NOTE*, with multi-expand accordion, save button may be clicked in non-selected panel...
        // TODO: handle that better, perhaps cause tool buttons to auto-select parent panel?
        // alternatively, could save changes to all Duties at once?...
        var pd = $scope.vm.Description;
        pd.ProposedJobId = $scope.ProposedJobClass ? $scope.ProposedJobClass.Value : undefined; // is there a better way???
        pd.TopJobClassSupervised = $scope.TopJobClass ? $scope.TopJobClass.Value : undefined;
        
        //        var posturl = appRoot + 'api/PositionDescription/' + pd.Id;
//        var posturl = $scope.appRoot + 'api/PositionDescription/' + pd.Id;
        var posturl = apiRoot + '/PositionDescription/' + pd.Id;
        try {
            $('#Message').html('Saving PDF #' + pd.Id + '...');
            //$scope.asyncOp = true;  (not used now...)
            //$scope.asyncNotification.show({ message: 'Saving PD...' }, 'async-op'); // info, success, warning, error...
            $http({
                method: 'PUT',
                url: posturl,
                data: pd,
                withCredentials: true
            })
            .success(function (data) {
                $timeout(function () {
//                    $scope.asyncNotification.hide();
                    $('#Message').html('');
                    if (cb) { cb(); }
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncNotification.hide();
                alert('ERROR:\n' + data.Message);
                //alert('ERROR:\n' + (data.Message || data.error)); // Controller sets error, AuthFilter returns Exception directly with Message...(see BaseApiAuthAttribute)
                //                if (data.error) { alert('ERROR: ' + data.error); };
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    ////$scope.selectDuty = function (item, $event) {
    ////    $scope.hideTasks();
    ////    $scope.selectedDuty = item;
    ////    // this seems necessary in IE8 for some reason, else new editor is opened disabled.
    ////    $("#dutyEdit").data("kendoEditor").focus();
    ////};

    ////$scope.selectedDutyId = 1234;
    //$scope.showTasks = function (item, $event) {
    //    if (item.TasksHtml) { // skip if blank
    //        // TODO: calculate correct display position, accounting for scroll positions, viewport space to left or right, etc.
    //        $('#DutyTasksPreview').html(item.TasksHtml);
    //        $('#DutyTasksPreview').css('left', $event.pageX + 16); // 16px right of cursor
    //        $('#DutyTasksPreview').css('top', $event.pageY + 16); // show 16px below cursor
    //        $('#DutyTasksPreview').fadeIn(200);
    //    }
    //};
    //// this is too early; template is not recompiled.  Just hardcoded Kendo widget classes into template, works.
    ////$scope.$watchCollection('vm.Duties', function (newval, oldval) {
    ////    // if new count > old count, rebuild panelbar...
    ////    // *NOTE*, this is a hack since Kendo PanelBar ignores collection changes...
    ////    if (newval.length > oldval.length) {
    ////        //alert('item added...');
    ////        //$scope.dutiesPanelBar = $('#DutiesPanelBar').kendoPanelBar($scope.options.DutyPanelBar);
    ////    }
    ////});

    $scope.updateApprovalTask = function (idx, status) {
        if (idx == 0 && status == 'Active') {
            // block / return false if form invalid...
            var valErrs = $scope.validate();
            if (valErrs.length) {
                alert('INPUT ERRORS:\n' + valErrs.join('\n'));
                return false;
            }
        }
        var statusAction = {
            'Active': 'Initiate',
            'Canceled': 'Skip',
            'Rejected': 'Reject',
            'Approved': 'Approve'
        };
        var cb = function () { // callback
            var t = $scope.vm.ApprovalTasks[idx]; // to avoid indexOf polyfill for IE8...
            //if (confirm('Approve?')) {
            // TODO: show asyncNotification here...
            // TODO: make save take callback
            //            $scope.save(function () {
            // TODO: bind Workflow status to completion of final step if displaying.
            //                t.ApprovalStatus = 'Approved';
            var updateTask = {};
            angular.copy(t, updateTask);
            updateTask.ApprovalStatus = status;
            $('#Message').html('updating Approval...');
//            $scope.asyncNotification.show({ message: 'updating Approval...' }, 'async-op'); // info, success, warning, error...
            $http({
                method: 'PUT',
                url: apiRoot + '/PdApproval/' + t.Id,
                data: updateTask,
                withCredentials: true
            })
            .success(function (d) { // save update returns only validation errors...
                $timeout(function () {
                    //$scope.asyncNotification.hide();
                    $('#Message').html('');
                    //console.log('CompletedDate:', d.CompletedDate); // this is not coming back as Date(nnn) now, but with embedded T.
                    t.CompletedDate = kendo.parseDate(d.CompletedDate);
//                    t.CompletedDate = formatJsonDate(d.CompletedDate);
                    t.ApproverFirstName = d.FirstName;
                    t.ApproverLastName = d.LastName;
                    t.ApprovalStatus = status;
                    if (idx < $scope.vm.ApprovalTasks.length && (status == 'Approved' || status == 'Canceled')) {
                        // set next task to active:
                        $scope.vm.ApprovalTasks[idx + 1].ApprovalStatus = 'Active';
                    }
                }, 300);
            })
            .error(function (data, status, headers, config, statusText) {
                $('#Message').html('');
//                $scope.asyncNotification.hide();
                //t.ApprovalStatus = origStatus;
                if (status == 500) {
                    alert('Error updating task status:\n' + data.Message);
                }
                else {
                    alert('Error on API call, status code ' + status);
                }
            });
        };
        if (confirm(statusAction[status] + '?')) {
            if (idx == 0 && status == 'Active') {
                $scope.savePD(cb);
            }
            else {
                cb();
            }
        }
    }

    $scope.validate = function () {
        var errs = [];
        // TODO: complete validation here, could call server, etc...
        if ($scope.dutyPctSum() != 100) {
            errs.push("Duty time allocation must sum to 100%");
        }
        return errs;
    }
})
.controller('PdListController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    //if (rq) { alert('runQuery = true'); }
    // TODO: include additional info (e.g. current user) in vm.
    // currently vm is a raw list.  result list should be a *property* of the vm.
    //$scope.timeout(function () {
    document.title = "Position Description Search";
    // show info on initial route:
    //if ($location.path().indexOf('/pd' == 0)) {
        //$scope.asyncNotification.show({ message: 'searching Position Descriptions...' }, 'async-op'); // info, success, warning, error...
    //}
    $scope.vm = fetch.data;
    // refresh data on browser history forward/back or search.
    $scope.$on('$locationChangeSuccess', function (event) {
        // *NOTE*, path check fails unless wrapped in timeout:
        $timeout(function () {
            var p = $location.path();
            if (!p || p.indexOf('/pd' != 0)) {
                return false;
            }
            $('#Message').html('searching Position Descriptions...');
        });
        var q = $location.search();
        var qs = [];
        for (var k in q) {
            qs.push(k + '=' + q[k]);
        }
        try {
            $http({ // promise
                method: 'GET',
                url: apiRoot + '/PositionDescription' + (qs.length ? '?' + qs.join('&') : ''),
                withCredentials: true
            }).success(function (d) {
                // moved hide() to grid.dataBound.
                //$timeout(function () {
                //    $scope.asyncNotification.hide();
                //}, 200);
                $scope.vm = d;
                // not really using this link, using data() method now instead.
                // http://stackoverflow.com/questions/18399805/reloading-refreshing-kendo-grid
                // data() wraps Results array in kendo.data.ObservableObject...
                $scope.pdfGrid.dataSource.data(d.Results);
                //http://www.telerik.com/forums/howto-reset-virtual-scrolling-on-datasource-query
                $timeout(function () {
                    if (d.Query.UpdatedSince) {
                        //                    d.Query.UpdatedSince = parseDate(d.Query.UpdatedSince);
                        d.Query.UpdatedSince = kendo.parseDate(d.Query.UpdatedSince);
                    }
                });
            })
            .error(function (data, status, errors, config) {
                //$scope.asyncNotification.hide();
                alert('http error.');
                //            if (data.error) { alert('ERROR: ' + data.error); }
            });
            //});
        }
        catch (xcp) {
            alert('xcp:' + JSON.stringify(xcp));
        }
    });
    // hide notification if exists...
    //console.log(Object.keys($location.search()).length);
    $scope.options = {
        AsyncNotification: {
            show: onShowAsyncNote,
            templates: [
                {
                    type: 'async-op',
                    template: $("#asyncOpTemplate").html()
                }
            ]
        },
        Grid: {
            columns: [
                {
                    field: "Id",
                    template: function (d) {
                        return "<a href='{{ appRoot }}#/pd/" + d.Id + "'>" + d.Id + "</a>"
                    },
                    title: "PDF #",
                    width: "40px"
                },
                {
                    field: "PositionId",
                    template: function (d) {
                        return d.PositionId ? "<a href='{{ appRoot }}#/position/" + d.PositionId + "'>" + d.PositionId + "</a>" : '';
                    },
                    title: "Pos #",
                    width: "40px"
                },
                //{
                //    field: "LegacyStatus",
                //    template: function (d) {
                //        return d.LegacyStatus || '';
                //    },
                //    title: "Status",
                //    width: "120px"
                //},
                {
                    field: "WorkingTitle",
                    template: function (d) {
                        //                        return d.WorkingTitle + ' [' + d.Incumbent + ']';
                        return d.WorkingTitle;
                    },
                    title: "Working Title",
                    width: "180px"
                },
                {
                    field: "IncumbentLast",
                    sortable: {
                        compare: function (a, b) {
                            // || '' to prevent null compare:
                            return (a.IncumbentLastName || '') < (b.IncumbentLastName || '') ?
                                -1 : (a.IncumbentLastName || '') > (b.IncumbentLastName || '') ? 1 : 0;
                        }
                    },
                    template: function (d) {
                        //                        return d.IsVacant ? '[VACANT]' : d.IncumbentLastName;
                        return (d.IncumbentLastName || '') + (d.IsVacant ? ' [VACANT]' : (d.EndDate ?
                            ' [' + $filter('date')(kendo.parseDate(d.EndDate), 'M/d/yy') + ']' :
                            (', ' + d.IncumbentFirstName)));
                    },
                    title: "Incumbent [thru]",
                    width: "120px"
                },
                //{
                //    field: "EndDate",
                //    template: function (d) {
                //        return d.EndDate ? $filter('date')(parseDate(d.EndDate), 'M/d/yy') : '';
                //    },
                //    title: "Thru",
                //    width: "60px"
                //},
                {
                    field: "ProposedClassCode",
                    template: function (d) {
                        return d.ProposedClassCode;
                    },
                    title: "Class Code",
                    width: "60px"
                },
                {
                    field: "LastUpdatedAt",
                    template: function (d) {
                        // TODO: parse all dates on load, consider moment.js date lib
                        var ds = kendo.parseDate(d.LastUpdatedAt);
                        // can't seem to get inline Angular date filter working here,
                        // so using script-style $filter:
                        //return kendo.toString(ds, 'd');
                        // https://docs.angularjs.org/api/ng/filter/date
                        return $filter('date')(ds, 'M/d/yy');
                    },
                    title: "Updated",
                    width: "70px"
                },
                {
                    field: "ApprovalStatusDesc",
                    template: function (d) {
                        return d.ApprovalStatusDesc || '';
                    },
                    title: "Approval Stage",
                    width: "120px"
                }
                //{
                //    field: "PdId",
                //    title: "Pos Desc",
                //    width: "80px"
                //},
                //{
                //    field: "EffectiveDate",
                //    //format: "{0:M/d/yyyy}",
                //    template: function (d) {
                //        //                        return "<a href='#/pd/" + d.PdId + "'>" + kendo.toString($scope.formatJsonDate(d.EffectiveDate), 'd') + "</a>";
                //        return d.PdId ? "<a href='#/pd/" + d.PdId + "'>" + kendo.toString(d.EffectiveDateString, 'd') + "</a>"
                //            : '[NONE]';
                //    },
                //    title: "Last PD",
                //    //type: "date",
                //    width: '80px'
                //}
            ],
            dataSource: {
                data: $scope.vm.Results,
                pageSize: 18
                //                type: "json"
            },
            dataBound: function (e) {
                $timeout(function () {
                    $('#Message').html('');
                }, 200);
                //$scope.asyncNotification.hide();
            },
            height: 550,
            //pageable: true,
            //rebind: $scope.vm.Results, doesn't seem to work...
            resizable: true,
            scrollable: {
                virtual: true
            },
            schema: {
                model: {
                    id: "Id"
                    //,fields: {
                    //    EffectiveDateString: {
                    //        type: "date"
                    //    }
                    //}
                }
                // see schema.parse for custom pre-parsing of vm
            },
            sortable: true
        }
    };

    $scope.createBlankPd = function () {
        //        var posturl = appRoot + 'api/PositionDescription';
        var posturl = apiRoot + '/PositionDescription';
        $('#Message').html('creating blank PDF...');
        //$scope.asyncNotification.show({ message: 'creating blank Position Descriptions...' }, 'async-op'); // info, success, warning, error...
        //if (confirm('This will create a new, blank Position Description.')) {
            $http({
                method: 'POST',
                url: posturl,
                data: {},
                withCredentials: true
            })
            .success(function (data) {
                //                data.LastUpdatedAt = formatJsonDate(data.LastUpdatedAt); // embedded list doesn't need this, only generated items.
                $timeout(function () {
                    $('#Message').html('');
//                    $scope.asyncNotification.hide();
  //                  alert('created PD # ' + data.Id + '; redirecting there now...');
                    // now redirect to new PD:
                    $location.path('/pd/' + data.Id);
                }, 300);
            }) // TODO: more robust client-side error handling...
                // also, standardize server-side error handling to include HTTP header, JSON error message, etc. for all
            .error(function (data, status, errors, config) {
                $('#Message').html('');
                alert('error:' + JSON.stringify(data));
                //console.log(data);
                //if (data.error) { alert('ERROR: ' + data.error); };
            });
//        }
    }
    $scope.runQuery = function () {
        // create query string from query object...
        //        alert('dataSource.data len:' + $scope.pdfGrid.dataSource.data().length);
        $timeout(function () {
//            $scope.asyncNotification.show({ message: 'searching Position Descriptions...' }, 'async-op'); // info, success, warning, error...
            $('#pdfGrid .k-scrollbar-vertical').scrollTop(0); // need to scrollTop *BEFORE* updating data source...looks like bug in Grid virtual scrolling
        });

        //        $('#pdfGrid .k-scrollbar-vertical').scrollTop(0); // need to scrollTop *BEFORE* updating data source...looks like 

        var q = $scope.vm.Query;
        var qp = []; // Query Params
        if (q.IncludeOld) qp.push('IncludeOld=true');
        if (q.DirectReportsOnly) qp.push('DirectReportsOnly=true');
        if (typeof q.UpdatedSince == 'object') { // Date, not string. (first submit goes as string.)
            q.UpdatedSince = $filter('date')(q.UpdatedSince, 'M/d/yy');
        }
        if (q.UpdatedSince) qp.push('UpdatedSince=' + q.UpdatedSince);
        if (q.QueryText) qp.push('QueryText=' + escape(q.QueryText));

        // TODO: handle when '/pd?[query string]'...
        if (qp.length) {
            //            $location.path('/pd').search( + (qp.length ? '?' + qp.join('&') : ''));
            //            $location.path('/pd').search(qp.join('&'));
            $location.search(qp.join('&')); // reloadOnSearch = false.
            //            $location.path('/pd/q/' + qp.join('&'));
        }
        else {
            $location.search('');
            //    //// per http://www.angularjshub.com/examples/routing/locationservice/
            //    //for (var k in $location.search()) {
            //    //    //$location.search(k, null); // only documented way to clear search...
            //    //}
        }
    }
})
.controller('PositionController', function ($scope, $filter, $http, $location, $rootScope, $route, $routeParams, $timeout, fetch) {
    $timeout(function () {
        $('#Message').html(''); // erase "Rendering..."
    }, 200);
    //    $scope.cpath = $fscope.path = ''; // initial path (appRoot/Position/{id})
    //$rootScope.$on('$locationChangeSuccess', function (event) {
    //    // TODO: single (flat) hash of all nodes by id
    //    $scope.path = $location.url(); // e.g. "/Contracts, /Delegations"
    //    $scope.cpath = $scope.path.substr(0, $scope.path.lastIndexOf('/'));
    //});
    // bypass init if vm already set:
    //alert('VM:' + $scope.vm); // this gets triggered TWICE on first load, is undefined initially.
    // TODO: apply this pattern on PdListController where no query exists.
    if (!$scope.vm) {
        //        $scope.vm = vm; // vm global is populated in .cshtml template
        $scope.vm = fetch.data;
        //$scope.appRoot = appRoot; // set in _Layout_ng.cshtml
        // for remembering expansion state during ngRoute navigation...
        // TODO: store this in a cookie.
        $scope.expandedItems = { 'PosInfo': true }; // store LI ids...default first panel to open.
        $scope.pbopts = { 'selectedItemId': null };
        $scope.selectedPdfId = null; // for holding PDF selection for actions like cloning, etc...
        $scope.selectedPdfAction = "none";
        //$scope.asyncMessage = ""; // e.g. CLONING, DELETING etc.

        //        $scope.selectedItemId = []; // this is a hack since changed scalar props are reverted during ngRoute follow...
        //TODO: convert CRUD ops to use ngResource and/or custom resource factory
        $scope.cloneLastPdf = function () {
            var clone = angular.copy($scope.vm.PositionDescriptions[0]);
            clone.ApprovalDate = undefined;
            clone.Status = 'Cloning...';
            $scope.vm.PositionDescriptions.unshift(clone);
            clone.isAdding = true;

            // TODO: connect this to back end Web API...this is just UI demo
            // (should be deep clone server-side...)
            // return new Id from back end asynchronously...
            $timeout(function () {
                $scope.asyncOp = false;
                clone.Status = 'New';
                clone.Id = Math.floor(Math.random() * 2000000000);
                clone.isAdding = false;
            }, 500);
        };

        $scope.clonePD = function (i) {
            var posturl = apiRoot + '/PositionDescription/' + $scope.vm.PositionDescriptions[i].Id + '/clone'
            try {
                $scope.asyncOp = true;
                $scope.asyncMessage = "CLONING"
                $http({
                    method: 'POST',
                    url: posturl,
                    withCredentials: true
                })
                .success(function (clone) {
                    $timeout(function () {
                        $scope.vm.PositionDescriptions.unshift(clone);
                        $scope.selectedPdfId = clone.Id;
                        $scope.asyncOp = false;
                    }, 500);
                })
                .error(function (data, status, errors, config) {
                    if (data.error) { alert('ERROR: ' + data.error); };
                    $scope.asyncOp = false;
                });
            }
            catch (xcp) {
                var err = xcp;
            }
        };
        // SEE PositionPdController for sample createPD, possible share in $rootScope?
        //        $scope.createPD = function () {
        //            alert('creating PD for position# ' + $scope.vm.Id);
        ////            var posturl = appRoot + 'api/PositionDescription';
        //        };
        $scope.createPD = function () {
            // TODO: add kendo Notification of async op status...
            var posturl = apiRoot + '/PositionDescription';
            if (confirm('This will create a new Description for Position #' + $scope.vm.Pos.Id
                + '.\nWhere possible, fields will be pre-populated from existing data.')) {
                //            var newPd = new PositionDescription($scope.vm.PositionId);
                var newPd = { PositionId: $scope.vm.Pos.Id };
                try {
                    $http({
                        method: 'POST',
                        url: posturl,
                        data: newPd,
                        withCredentials: true
                    })
                    .success(function (data) {
                        data.LastUpdatedAt = formatJsonDate(data.LastUpdatedAt); // embedded list doesn't need this, only generated items.
                        $timeout(function () {
                            // unshift this to PD list here instead...
                            $scope.vm.PositionDescriptions.unshift(data);
                        }, 300);
                    }) // TODO: more robust client-side error handling...
                    .error(function (data, status, errors, config) {
                        alert('error:' + JSON.stringify(data));
                        //console.log(data);
                        //if (data.error) { alert('ERROR: ' + data.error); };
                    });
                }
                catch (xcp) {
                    alert('POST ERROR:' + JSON.stringify(xcp));
                }
            }
        };

        $scope.deletePD = function (i) {
            var posturl = apiRoot + '/PositionDescription/' + $scope.vm.PositionDescriptions[i].Id
            // TODO: allow deleting only PDs in "created" approval state...
            if (!confirm('This will permanently delete PDF #' + $scope.vm.PositionDescriptions[i].Id
                + ', along with any associated Duty allocations.')) { return false; };
            // TODO: put this into AsyncOpIndicator...
            //$scope.vm.PositionDescriptions[i].Status = "Deleting...";
            $scope.vm.PositionDescriptions[i].isDeleting = true;
            $scope.asyncOp = true;
            $scope.asyncMessage = "DELETING"

            $http({
                method: 'DELETE',
                url: posturl,
                withCredentials: true
            })
            .success(function (deleted) {
                // this seems to succeed silently for IE...
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.vm.PositionDescriptions.splice(i, 1);
                }, 300); // 300ms timeout to ensure deletion is visible...
            })
            .error(function (data, status, errors, config) {
                alert('server error.');
            });

            //$timeout(function () {
            //    $scope.asyncOp = false;
            //    $scope.vm.PositionDescriptions.splice(i, 1);
            //}, 500);
        };

        $scope.doParAction = function (actionCode, itemType) {
            if (actionCode == '') return false;
            switch (actionCode) {
                case 'P0':
                    $scope.createPar({
                        //                    PosSupervisorPosId: posVm.Id(),
                        PosSupervisorPosId: $scope.vm.Pos.Id,
                        ActionCode: actionCode
                    });
                    break;
                case 'U3-81-in':
                    $scope.createPar({
                        //                    PositionId: posVm.Id(),
                        PositionId: $scope.vm.Pos.Id,
                        ActionCode: 'U3',
                        ActionReasonId: 81,
                        IsTransferIn: true
                    });
                    break;
                default: $scope.createPar({
                    //                PositionId: posVm.Id(),
                    PositionId: $scope.vm.Pos.Id,
                    // PositionId: n.attr('id'),
                    // EmployeeId is irrelevant here:
                    //                EmployeeId: (itemType == 'Emp') ? n.data('IncumbentId') : null,
                    //ActionType: actionCode
                    ActionCode: actionCode
                });
            }
        }

        $scope.createPar = function (parData) {
            // TODO: test that parData is not null and is an object...
            var posturl = apiRoot + '/par';
            $timeout(function () {
                $scope.asyncNotification.show({ message: 'Creating PAR...' }, 'async-op'); // info, success, warning, error...
            }); // *this* timeout is to ensure Angular $apply.  Next one is for delay to ensure visibility before redirect.
            $timeout(function () {
                try {
                    $http({ method: 'POST', url: posturl, data: parData })
                    .success(function (d) {
                        window.location = appRoot + 'par/' + d.Par.Id;
                    })
                    .error(function (d, status, errors, config) {
                        $scope.asyncNotification.hide();
                        alert('ERROR! ' + d);
                        if (d.error) { alert('ERROR: ' + d.error); };
                    });
                }
                catch (xcp) {
                    var err = xcp;
                    alert('ERROR: ' + JSON.stringify(xcp));
                }
            }, 300);
        }

        $scope.doPdAction = function (newscope, idx) {
            switch (newscope.selectedPdAction) {
                case 'clone': {
                    $scope.clonePD(idx);
                    return;
                }
                case 'delete': {
                    $scope.deletePD(idx);
                    return;
                }
                default: return;
            }
        }

        $scope.getIndentStyle = function (p) {
            return {
                'padding-left': (($scope.vm.ChainOfCommand[0].Level - p.Level) * 1.5 + 'em')
            };
        };

        // config options for KendoUI widgets:
        // TODO: move this out of if block, dynamically set expandMode to multiple only if > 1 currently expanded?
        //$scope.options = {
        //    PanelBar: {
        //        animation: {
        //            collapse: {
        //                duration: 300
        //            },
        //            expand: {
        //                duration: 300,
        //                effects: 'expandVertical'
        //            }
        //        },
        //        expandMode: ($scope.expandedItems.length) ? "multiple" : "single",
        //        //            expandMode: (kc == 2) ? "multiple" : "single",
        //        collapse: function (pb) {
        //            delete $scope.expandedItems[$(pb.item).attr('id')];
        //        },
        //        expand: function (pb) {
        //            $scope.expandedItems[$(pb.item).attr('id')] = true;
        //        },
        //        select: function (pb) {
        //            $scope.selectedItemId = $(pb.item).attr('id');
        //        }
        //    }
        //};

        $scope.performAction = function (selectorId) {
            var action = $('#' + selectorId).val();
            if (action != '') { // skip if no selection
                //                doAction(action, 'Pos'); // always Pos in this context, can vary in treeview context
                $scope.doParAction(action, 'Pos'); // always Pos in this context, can vary in treeview context
            }
        }
        $scope.formatJsonDate = function (d) { return formatJsonDate(d) };

        $scope.rowClicked = function (pdr) {
            $scope.selectedPdfId = pdr.Id;
        }
    } // pre-init check, irrelevant now...

    document.title = 'Position #' + $scope.vm.Pos.Id + ' Detail';

    $scope.options = {
        AsyncNotification: {
            show: onShowAsyncNote,
            templates: [
                {
                    type: 'async-op',
                    template: $("#asyncOpTemplate").html()
                }
            ]
        },
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            expandMode: $scope.pbopts.expandMode || 'single',
            collapse: function (pb) {
                delete $scope.expandedItems[$(pb.item).attr('id')];
            },
            expand: function (pb) {
                $scope.expandedItems[$(pb.item).attr('id')] = true;
            },
            select: function (pb) {
                $scope.pbopts.selectedItemId = $(pb.item).attr('id');
            }
        }
    };

    // these always needs initialization due to panelbar directive:
    $scope.collapseAll = function () {
        $scope.pbopts.expandMode = $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse('li');
    };

    $scope.expandAll = function () {
        // *NOTE, panelbar gets replaced on re-render, thus caching as separate property here.
        $scope.pbopts.expandMode = $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand('li');
    };
});