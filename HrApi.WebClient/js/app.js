﻿//var apiRoot = "http://apps-dev.cts.wa.gov/hrapi/api"; // TODO: getApiRoot() func
var apiRoot = "http://localhost:12130/api"; // TODO: getApiRoot() func
//var apiRoot = "http://localhost/hrapi/api";
//var asyncNote;

var app = angular.module('app', ['ngRoute', 'kendo.directives'])
.controller('MainController', function ($http, $route, $filter, $rootScope, $scope, $timeout, $location) {
    $rootScope.appRoot = getAppRoot();
    $rootScope.apiRoot = apiRoot;
    // TODO: give up on AsyncNotification - slow, clunky.  just use jquery $('#Message') instead.
    $rootScope.options = {
        AsyncNotification: {
            show: onShowAsyncNote,
            stacking: "down",
            templates: [
                {
                    type: 'async-op',
                    template: $("#asyncOpTemplate").html()
                }
            ]
        }
    };
    try {
        // call login controller:
        $('#Message').html('logging in...');
        $http({
            method: 'GET',
            url: apiRoot + '/AppUser',
            withCredentials: true
        })
        .success(function (d) { // TODO: return additional UserInfo here, without allowing identity-forging mechanism
            $('#Message').html('');
            $rootScope.user = d;
        })
        .error(function (data, status, errors, config) {
            $('#Message').html('login failed...');
        });
    }
    catch (xcp) {
        alert('network error');
    }
})
.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'ng-templates/home.html'
        //, controller: 'HomeController'
    })
    .when('/impersonation', {
        templateUrl: 'ng-templates/impersonation.html',
        controller: 'ImpersonationController',
        caseInsensitiveMatch: true,
        resolve: {
            fetch: function ($route, $http, $location) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                return $http({ // promise
                    method: 'GET',
                    url: apiRoot + '/Impersonation',
                    withCredentials: true
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    if (data.error) { alert('ERROR: ' + data.error); }
                });
            }
        }
    })
    .when('/par', {
        // TODO: use getAppRoot(pd) for template root...
        templateUrl: 'ng-templates/par.html',
        controller: 'ParListController',
        caseInsensitiveMatch: true,
        resolve: {
            fetch: function ($route, $http, $location, $timeout) {
                // using jQuery / DOM for asyncNote since no $scope access here...
                //$('#notifier').kendoNotification().data('kendoNotification').show({ message: 'loading PAR list...' }, 'async-op'); // info, success, warning, error...
                //$('#notifier').kendoNotification().data('kendoNotification').show({ message: 'loading PAR list...' }, 'async-op'); // info, success, warning, error...

                // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //console.log("scope in rout:", $scope);
                // DOM manipulation...mea culpa!
                $('#Message').html('loading...');
                //                $rootScope.asyncNotification.show({ message: 'searching Position Descriptions...' }, 'async-op'); // info, success, warning, error...
                var qs = [];
                //if (Object.keys($location.search()).length) {
                for (var k in $location.search()) {
                    qs.push(k + '=' + $location.search()[k]);
                }
                //}
                return $http({ // promise
                    method: 'GET',
                    url: apiRoot + '/Par' + (qs.length ? '?' + qs.join('&') : ''),
                    withCredentials: true
                }).success(function (data) {
                    $timeout(function () {
                        $('#Message').html('rendering...');
                    }, 100);
                    return data;
                })
                .error(function (data, status, errors, config) {
                    //$rootScope.asyncNotification.hide();
                    alert('http error');
                    //if (data && data.error) { alert('ERROR: ' + data.error); }
                });
            }
        }
    })
    .when('/par/:id', {
        // TODO: use getAppRoot(pd) for template root...
        templateUrl: 'ng-templates/par-detail.html',
        controller: 'ParController',
        caseInsensitiveMatch: true,
        resolve: {
            fetch: function ($route, $http, $location, $timeout) {
                // DOM manipulation...mea culpa!
                $('#Message').html('loading...');
                return $http({ // promise
                    method: 'GET',
                    url: apiRoot + '/par/' + $route.current.params.id,
                    withCredentials: true
                }).success(function (data) {
                    $('#Message').html('rendering...');
                    return data;
                })
                .error(function (data, status, errors, config) {
                    console.log(data);
                    $('#Message').html('');
                    alert('http error');
                });
            }
        }
    })
    .when('/pd', {
        // TODO: use getAppRoot(pd) for template root...
        templateUrl: 'ng-templates/pdq.html',
        controller: 'PdListController',
        caseInsensitiveMatch: true,
        reloadOnSearch: false,
        resolve: {
            fetch: function ($route, $http, $location) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //console.log("scope in rout:", $scope);
                // DOM manipulation...mea culpa!
                $('#Message').html('loading...');
//                $rootScope.asyncNotification.show({ message: 'searching Position Descriptions...' }, 'async-op'); // info, success, warning, error...
                var qs = [];
                //if (Object.keys($location.search()).length) {
                for (var k in $location.search()) {
                    qs.push(k + '=' + $location.search()[k]);
                }
                //}
                return $http({ // promise
                    method: 'GET',
                    url: apiRoot + '/PositionDescription' + (qs.length ? '?' + qs.join('&') : ''),
                    withCredentials: true
                }).success(function (data) {
                    //$rootScope.asyncNotification.hide();
                    $('#Message').html('rendering...');
                    return data;
                })
                .error(function (data, status, errors, config) {
                    //$rootScope.asyncNotification.hide();
                    $('#Message').html('');
                    alert('http error');
                    //if (data && data.error) { alert('ERROR: ' + data.error); }
                });
            }
        }
    })
    .when('/pd/:id', {
        templateUrl: 'ng-templates/position-description.html', // TODO: rename this to PdDetail for consistency
        caseInsensitiveMatch: true,
        controller: 'PdController',
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                $('#Message').html('loading...');
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: apiRoot + '/PositionDescription/' + id,
                    withCredentials: true
                }).success(function (data) {
                    $('#Message').html('');
                    return data;
                })
                .error(function (data, status, errors, config) {
                    $('#Message').html('');
                    if (data.error) { alert('ERROR: ' + data.error); }
                });
            }
        }
    })
    .when('/position/:id', {
        caseInsensitiveMatch: true,
        templateUrl: 'ng-templates/position.html',
        controller: 'PositionController', // client controller (already existed, trying to convert NgPosDetail.cshtml to Partial)
        resolve: {
            fetch: function ($route, $http) {
                $('#Message').html('loading...');
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: apiRoot + '/position/' + id,
                    withCredentials: true
                }).success(function (data) {
                    $('#Message').html('rendering...');
                    return data;
                })
                .error(function (data, status, errors, config) {
                    $('#Message').html('');
                    alert('get data failed in PositionController: ' + eval(data));
                });
            }
        },
    })
    $locationProvider.html5Mode(false);
});

// I think there is something like this in kendo??? use that if so.  Oh no, that's parseDate.  different thing.
function formatJsonDate(d) {
    // allow negation prefix for dates before 1970:
    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

function getAppRoot() {
    // hack for IE8 missing window.location.origin:
    // http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    // *NOTE*, approot can be /apps and call /hr/api/...
    //alert(window.location.pathname);
    return window.location.origin + window.location.pathname;
}

// TODO: hardcode this anonymously into options config??
function onShowAsyncNote(e) {
    // calculate correct position (top center) for async note:
    if (!$("." + e.sender._guid)[1]) {
        var element = e.element.parent(),
            eWidth = element.width(),
            eHeight = element.height(),
            wWidth = $(window).width(),
            wHeight = $(window).height(),
            newTop, newLeft;

        newLeft = Math.floor(wWidth / 2 - eWidth / 2);
        newTop = Math.floor(wHeight / 2 - eHeight / 2);

        // CENTER CENTER:
        //e.element.parent().css({ top: newTop, left: newLeft });
        // TOP CENTER:
        e.element.parent().css({ top: 10, left: newLeft });
    }
}