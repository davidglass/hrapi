﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HrApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // DG: enabling Cors...
            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "GetDetail",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { action = "Detail" }, // *NOTE*, id param is *not* optional for this route
            //        constraints: new { id = @"\d+" }
            //    ); // id d+ constraint added to prevent matching /controller/new

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Clone",
                routeTemplate: "api/{controller}/{id}/clone",
                defaults: new
                {
                    action = "Clone" // clones referenced item, returns clone (with new ID) in reponse...
                }
            );
        }
    }
}
