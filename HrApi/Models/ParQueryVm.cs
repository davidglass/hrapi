﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DbUtil;

namespace HrApi.Models
{
    public class ParQueryVm : ModelBase
    {
        public ParQueryVm()
        {
            Init();
        }

        private void Init()
        {
            ActionTypes = Context.GetByProc("GetParActionTypes", new { });
            ApprovalStatuses = Context.GetByProc("GetApprovalStatusSelection", new { });
        }

        public void Get()
        {
            var ui = Context.GetByProc("GetUserInfoNew", new { LanId = User.Identity.Name })[0];
            Query.LanId = ui["LanId"].ToString();
//            Query.LanId = User.Identity.Name;
            Results = Context.GetByProc("GetParList", Query );
        }

        public ParQuery Query { get; set; }
//        public List<PdqResult> Results { get; set; }
        public List<Dictionary<string, object>> ActionTypes { get; set; }
        public List<Dictionary<string, object>> ApprovalStatuses { get; set; }
        public List<Dictionary<string, object>> Results { get; set; }
    }

    public class ParQuery
    {
        public string ActionCode { get; set; }
        public int OrgUnitId { get; set; }
        // TODO: convert single-select-option Ids to KeyVal objects for simple Angular binding...
        public int ParStatusId { get; set; }
        public int WorkflowStatusId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        // TODO: refactor "Query" prop to "SearchString": public string SearchString { get; set; }
        public string Query { get; set; }
        public string LanId { get; set; }
    }
}