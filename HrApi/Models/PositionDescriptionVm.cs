﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;


namespace HrApi.Models
{
    public class PositionDescriptionVm: ModelBase
    {
        public PositionDescriptionVm(IPrincipal user, int id)
        {
            Init(user, id);
        }

        private void Init(IPrincipal user, int id)
        {
            // TODO: implement AccessCheck (from HrAdmin2 Api/PositionController) in BaseApiController...
            Description = Context.GetByProc("GetRecordById", new { TableName = "PositionDescription", Id = id })[0];
            Duties = Context.GetByProc("GetPositionDuties", new { PositionDescriptionId = id });
            ApprovalTasks = Context.GetByProc("GetPdApprovalTasks", new { PdId = id });
            IncumbentName = (string)Context.GetByProc("GetPdIncumbentName", new { PdId = id })[0]["IncumbentName"];
            JobClasses = Context.GetByProc("GetJobClassSelection", new { });
        }

        public List<Dictionary<string, object>> ApprovalTasks { get; set; }
        public Dictionary<string, object> Description { get; set; }
        public List<Dictionary<string, object>> Duties { get; set; }
        public string IncumbentName { get; set; }
        public List<Dictionary<string, object>> JobClasses { get; set; }

    }
}