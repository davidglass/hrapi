﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DbUtil;

namespace HrApi.Models
{
    /// <summary>
    /// Position Description Query View Model
    /// holds current query parameters and Results...
    /// </summary>
    public class PdqVm : ModelBase
    {
        private List<Dictionary<string,object>> rs;
        private void RunQuery()
        {
            var ui = Context.GetByProc("GetUserInfoNew", new { LanId = User.Identity.Name })[0];
            Query.LanId = ui["LanId"].ToString();
            var rs = new ActiveRecord
            {
                Context = Context,
                Source = "GetPdList",
                SourceType = ActiveRecord.EntityType.Proc,
                RecordParams = Query
            };
            Results = rs.GetAll();
        }

        public PdQuery Query { get; set; }
        public List<Dictionary<string,object>> Results {
            get
            {
                if (rs == null)
                {
                    RunQuery();
                }
                return rs;
            }
            private set {
                rs = value;
            }
        }
    }

    public class PdQuery
    {
        public string LanId { get; set; }
        public bool DirectReportsOnly { get; set; }
        public bool IncludeOld { get; set; }
        public string QueryText { get; set; }
        public DateTime? UpdatedSince { get; set; }
    }
}