﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

namespace HrApi.Models
{
    public class PositionVm : ModelBase
    {
        public PositionVm(IPrincipal user, int id)
        {
            Init(user, id);
        }

        private void Init(IPrincipal user, int id)
        {
            var ui = Context.GetByProc("GetUserInfoNew", new { LanId = user.Identity.Name })[0];
            // TODO: untyped GetSingleByProc...
//            Pos = Context.GetByProc("GetPositionDetail", new { PosId = id, LanId = user.Identity.Name })[0];
            Pos = Context.GetByProc("GetPositionDetail", new { PosId = id, LanId = ui["LanId"].ToString() })[0];
            ApptHistory = Context.GetByProc("GetPositionApptHistory", new { PositionId = id });
            PositionDescriptions = Context.GetByProc("GetPositionDescriptions", new { PositionId = id });
            ChainOfCommand = Context.GetByProc("GetChainOfCommandWithDirectReports", new { Id = id });
//            var chain = Context.GetByProc<ChainLink>("GetChainOfCommandWithDirectReports", parameters: new { Id = id });
            //ChainOfCommand = new ChainLinkVm(chain);
            //OpenPars = db.GetByProc<Par>("GetOpenPars", parameters: new { PositionId = Pos.Id });
        }
        public Dictionary<string, object> Pos { get; set; }
        public List<Dictionary<string, object>> ApptHistory { get; set; }
        public List<Dictionary<string, object>> PositionDescriptions { get; set; }
        public List<Dictionary<string, object>> ChainOfCommand { get; set; }
    }
}