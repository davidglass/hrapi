﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Data.Entity;

using AuthFilter;

namespace HrApi.Models
{
    public class PositionDescription : ModelBase
    {
        public PositionDescriptionRow Clone()
        {
            if (PositionId.HasValue && !HasAccess(ModelAction.Create))
            {
                throw new Exception("Unauthorized.");
            }
            return Context.GetSingleByProc<PositionDescriptionRow>("ClonePositionDesc", new { PdId = Id, LanId = User.Identity.Name });
        }

//        public override PositionDescriptionRow Create()
        public PositionDescriptionRow Create()
        {
            if (PositionId.HasValue && !HasAccess(ModelAction.Create))
            {
                throw new Exception("Unauthorized.");
            }

            PositionDescriptionRow newPd = new PositionDescriptionRow();

            // LinkedParId is set when generating new Pdf from PAR:
            // TODO: separate proc for CreatePdFromModifyPar (no PD approval gen)
            if (LinkedParId.HasValue)
            {
                newPd = (PositionId.HasValue) ?
                    Context.GetSingleByProc<PositionDescriptionRow>("CreatePdFromPar", new
                    {
                        PositionId = PositionId.Value,
                        LanId = User.Identity.Name,
                        LinkedParId = LinkedParId.Value,
                        WorkingTitle = WorkingTitle,
                        ProposedJobId = ProposedJobId,
                        PositionTypeCXW = PositionTypeCXW,
                        xProposedJvacEvalPts = xProposedJvacEvalPts,
                        IsSupervisor = IsSupervisor
                    })
                    : Context.GetSingleByProc<PositionDescriptionRow>("CreateBlankPositionDescription", new
                    {
                        LanId = User.Identity.Name,
                        LinkedParId = LinkedParId.Value,
                        WorkingTitle = WorkingTitle,
                        ProposedJobId = ProposedJobId,
                        PositionTypeCXW = PositionTypeCXW,
                        xProposedJvacEvalPts = xProposedJvacEvalPts,
                        IsSupervisor = IsSupervisor
                    });
            }

            else
            {
                // *NOTE*, blank PD requires no approval.
                // New Position PAR approval linked to blank PD would suffice.
                newPd = (!PositionId.HasValue) ? Context.GetSingleByProc<PositionDescriptionRow>("CreateBlankPositionDescription", new { LanId = User.Identity.Name })
                : Context.GetSingleByProc<PositionDescriptionRow>("CreatePdWithApproval", new
                {
                    PositionId = PositionId,
                    LanId = User.Identity.Name
                });
            }
            return newPd;
        }

        public void Delete()
        {
            if (!Context.Exec("DeleteRecordById", new { TableName = "PositionDescription", Id = Id }))
            {
                throw new Exception("Delete Position Description failed.");
            }
        }

        public void Update()
        {
            try
            {
                if (!HasAccess(ModelAction.Update))
                {
                    // TODO: return HRM with Unauthorized status?
                    throw new Exception("Unauthorized.");
                }

                Context.PositionDescriptions.Attach(this);
                // *NOTE*, from EF5 to EF6, Data.EntityState moved to Data.Entity.EntityState:
                Context.Entry(this).State = System.Data.Entity.EntityState.Modified;
                var updated = Context.SaveChanges();            
                if (updated != 1) {
                    throw new Exception("Nothing updated.");
                }
            }
            catch (Exception xcp)
            {
                throw new Exception("Update Failed...", xcp);
            }
        }

        // TODO: move this into base class, inject AccessCheck sproc name...
        private bool HasAccess(ModelAction xn)
        {
            // TODO: get ActionFlags for current user, ModelAction.
            var permitted = ActionFlags.Read;
            if (xn != ModelAction.Read && xn != ModelAction.Create)
            {
                // if PD Workflow status is Created and Position is not own or user is Admin/HR, add Write+Delete:
                var pmal = Context.GetByProc<ModelActionVal>("GetPdPermittedActions", new { PdId = Id, LanId = User.Identity.Name });
                foreach (var pma in pmal)
                {
                    if (!pma.Value.HasValue) { continue; } // skip nulls
                    permitted = permitted | (ActionFlags)pma.Value.Value;
                }

                if (!permitted.HasFlag((ActionFlags)xn))
                {
                    return false;
                }
            }

            return Context.GetSingleByProc<AccessCheck>("PdPosIsInSpanOfControl", new
            {
                PdId = Id,
                LanId = User.Identity.Name
            }).IsSub;
        }

        public int Id { get; set; }
        public int? PositionId { get; set; }
        public string PositionObjective { get; set; }
        public int? ProposedJobId { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
        public bool IsLead { get; set; }
        public decimal? DirectFteSupervised { get; set; }
        public decimal? TotalFteSupervised { get; set; }
        public int? TopJobClassSupervised { get; set; }
        public bool IsSupervisor { get; set; }
        public bool AssignsWork { get; set; }
        // TODO: finish this...possibly add separate tables for Wms, Exempt??? or prefix column names
        public bool Instructs { get; set; }
        public bool ChecksWork { get; set; }
        public bool PlansWork { get; set; }
        public bool EvalsPerformance { get; set; }
        public bool Disciplines { get; set; }
        public bool Hires { get; set; }
        public bool Terminates { get; set; }
        public string SupervisorLeadAddInfo { get; set; }
        public int SupervisionLevel { get; set; } // TODO: enum for this...
        public string SupervisionAddInfo { get; set; }
        public bool IsCoopCritical { get; set; }
        public string CoopSupportFunctions { get; set; }
        public string WorkSetting { get; set; }
        public string WorkSchedule { get; set; }
        public string TravelReqs { get; set; }
        public string ToolsEquipment { get; set; }
        public string CustomerInteraction { get; set; }
        public string WorkingConditionsOther { get; set; }
        public string QualificationsRequired { get; set; }
        public string QualificationsPreferred { get; set; }
        public string SpecialRequirements { get; set; }
        public string InTrainingPlan { get; set; }
        public string PositionTypeCXW { get; set; }

        // w prefix is WMS-only:
        //public string wAccountabilityControlInfluence { get; set; } // removed this as redundant with supervision and budget info.
        public string wAccountabilityScope { get; set; }
        public string wDecisionExpertise { get; set; }
        public string wDecisionsRisk { get; set; }
        public string wDecisionsTacticalStrategic { get; set; }

        // wx prefix applies to WMS and eXempt, x to eXempt only
        public string wxDecisionPolicyImpact { get; set; }
        public string wxDecisionResponsibility { get; set; }
        public string wxDecisionsUnauthorized { get; set; }
        public string wxFinancialBudget { get; set; }
        public string wxFinancialImpact { get; set; }
        // wxScopeOfControl was replaced with wAccountabilityControlInfluence.
        // (field is unused by Exempt, *also redundant* with other fields)
        //public string wxScopeOfControl { get; set; }
        //public string xPolicyImpact { get; set; }
        public string xCurrentBand { get; set; }
        public int? xCurrentJobId { get; set; } // this could easily be set for all (not just eXempt)
        // xMgmtCode is actually a char? in db:
        public string xMgmtCodePMC { get; set; }
        public string xProposedBand { get; set; }
        public int? xCurrentJvacEvalPts { get; set; }
        public int? xProposedJvacEvalPts { get; set; }
        public string xExemptCitationHeading { get; set; }
        public int? LinkedParId { get; set; }
    }

    public class PositionDescriptionRow : ModelBase
    {
        public int Id { get; set; }
        public int PositionId { get; set; }
        public int ProposedJobId { get; set; }
        public string ProposedClassCode { get; set; }
        public string ProposedClassTitle { get; set; }
        public string Incumbent { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
    }

//    public class PositionDuty : ModelBase
    public class PositionDuty : ModelBase
    {
        public PositionDuty Create()
        {
            if (!HasAccess(PositionDescriptionId, ModelAction.Create))
            {
                throw new Exception("Unauthorized.");
            }

            // hack since EF doesn't support default next value sequences:
            // sequence returns Int64, not Int32 (should be safe to cast for a while)
            Id = (int)Context.Database.SqlQuery<Int64>("select next value for dbo.PositionDuty_seq").FirstOrDefault();
            Context.PositionDuties.Add(this);
            int saved = Context.SaveChanges();
            return this;
        }

        public void Delete()
        {
            if (!Context.Exec("DeleteRecordById", new { TableName = "PositionDuty", Id = Id }))
            {
                throw new Exception("Delete Position Duty failed.");
            }
        }

        public void Update()
        {
            if (!HasAccess(PositionDescriptionId, ModelAction.Update))
            {
                throw new Exception("Unauthorized.");
            }
            Context.PositionDuties.Attach(this);
            var e = Context.Entry<PositionDuty>(this);
            // *NOTE*, EntityState moved from System.Data to System.Data.Entity in EF6 vs EF5.
            e.State = System.Data.Entity.EntityState.Modified;
            var updated = Context.SaveChanges();
            if (updated < 1)
            {
                throw new Exception("PositionDuty update failed...");
            }
        }

        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int Id { get; set; } // sequence returns long, being cast to int.
        public int PositionDescriptionId { get; set; }
        public string Duty { get; set; }
        public string TasksHtml { get; set; }
        public int TimePercent { get; set; }

        // TODO: ModelBase.HasAccess(ModelAction, EntityName, Id)...
        // *NOTE*, this version applies PD access permissions to PD *Duties*. 
        private bool HasAccess(int pdfId, ModelAction xn)
        {
            // TODO: get ActionFlags for current user, ModelAction.
            var permitted = ActionFlags.Read;
            if (xn != ModelAction.Read && xn != ModelAction.Create)
            {
                // if PD Workflow status is Created and Position is not own or user is Admin/HR, add Write+Delete:
                var pmal = Context.GetByProc<ModelActionVal>("GetPdPermittedActions", new { PdId = pdfId, LanId = User.Identity.Name });
                foreach (var pma in pmal)
                {
                    if (!pma.Value.HasValue) { continue; } // skip nulls
                    permitted = permitted | (ActionFlags)pma.Value.Value;
                }

                if (!permitted.HasFlag((ActionFlags)xn))
                {
                    return false;
                }
            }

            return Context.GetSingleByProc<AccessCheck>("PdPosIsInSpanOfControl", new
            {
                PdId = Id,
                LanId = User.Identity.Name
            }).IsSub;
        }
    }

    public class ModelActionVal
    {
        public int? Value { get; set; }
    }

    public class PdIncumbent
    {
        public string IncumbentName { get; set; }
    }

    public class AccessCheck
    {
        public bool IsSub { get; set; }
    }
}

//namespace HrApi.Models.ViewModels
//{
//    public class PositionDescriptionVm : BaseVm
//    {
//        public PositionDescription Description { get; set; }
//        public List<PositionDuty> Duties { get; set; }
//        public List<ApprovalTaskPd> ApprovalTasks { get; set; }
//        // TODO: make this List<Employee> or such?, show Incumbent(s) from PD active period?
//        public string IncumbentName { get; set; }
//    }
//}