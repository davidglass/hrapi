﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

namespace HrApi.Models
{
    public class ImpersonationVm : ModelBase
    {
        private int? impUserId;
        public ImpersonationVm()
        {
            Users = Context.GetByProc("GetUserEmpSelection", new { });
        }
        public List<Dictionary<string, object>> Users { get; set; }
        public int? ImpersonatingUserId {
            get
            {
                if (impUserId == null)
                {
                    var impersonating = Context.GetByProc("GetImpersonatedUserId", new { LanId = User.Identity.Name });
                    if (impersonating.Count > 0)
                    {
                        impUserId = (int)impersonating[0]["ImpersonatingUserId"];
                    }
                }
                return impUserId;
            }
            set
            {
                impUserId = value;
            }
        }

        public void Update()
        {
            if (!Context.Exec("UpdateImpersonation", new { LanId = User.Identity.Name, ImpersonatedUserId = impUserId }))
            {
                throw new Exception("Impersonation Update failed.");
            }
        }

        public Dictionary<string,object> GetImpersonatedUser()
        {
            if (!ImpersonatingUserId.HasValue)
            {
                return Context.GetByProc("GetUserInfoNew", new { LanId = User.Identity.Name })[0];
            }
            var iu = Context.GetByProc("GetUserInfoById", new { Id = ImpersonatingUserId.Value })[0];
            iu["RealLanId"] = User.Identity.Name;
            return iu;
        }
    }
}