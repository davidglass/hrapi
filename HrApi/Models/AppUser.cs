﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

namespace HrApi.Models
{
    public class AppUser : ModelBase
    {
        private Dictionary<string, object> ui;
//        public AppUser(IPrincipal user)
        public Dictionary<string, object> UserInfo {
            get
            {
                if (ui == null)
                {
                    ui = Context.GetByProc("GetUserInfoNew", new { LanId = User.Identity.Name })[0];
                }
                return ui;
            }
            set
            {
                ui = value;
            }
        }
    }
}