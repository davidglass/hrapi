﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using System.Web.Script.Serialization; // in System.Web.Extensions
//using DbUtil;
using HrApi.Models;
using System.ComponentModel.DataAnnotations.Schema; // for NotMapped.

namespace HrApi.Models
{
    public class ModelBase
    {
        //[ScriptIgnore]
        // THIS MUST BE PROTECTED to prevent serialization attempts.  ScriptIgnore is ignored!?
        [NotMapped]
        protected HrApiContext Context = new HrApiContext("DbConnStr");
        // TODO: Create, Update, Delete methods?
        [NotMapped]
        public System.Security.Principal.IPrincipal User { get; set; }

        // TODO: T Create<T>() generic
//        public virtual T Create<T>()
        //public virtual ModelBase Create()
        //{
        //    throw new NotImplementedException("virtual Create");
        //}
    }
}