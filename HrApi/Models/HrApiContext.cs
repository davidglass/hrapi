﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using DbUtil;

namespace HrApi.Models
{
    public class HrApiContext : SmartDbContext
    {
        public HrApiContext(string connstr) : base(connstr) {
        }
        public DbSet<PositionDescription> PositionDescriptions { get; set; }
        public DbSet<PositionDuty> PositionDuties { get; set; }
    }
}