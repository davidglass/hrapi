﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RazorEngine;

namespace HrApi.Models
{
    public class ApprovalTaskPd : ModelBase
    {
        public string WorkingTitle { get; set; }
        public string FirstName { get; set; } // currently appointed approver
        public string LastName { get; set; }
        public string Comment { get; set; }
        public string ApprovalStatus { get; set; }
        public string ApproverFirstName { get; set; }
        public string ApproverLastName { get; set; }
        public int? ApproverPositionId { get; set; }
        public DateTime? CompletedDate { get; set; }
    }

    public class ApprovalTaskUpdate: ModelBase
    {
        public TaskApprover Update()
        {
            UpdaterLanId = User.Identity.Name;
            var approvedBy = Context.GetSingleByProc<TaskApprover>("UpdateApprovalStatus", parameters: new
            {
                Id = this.Id,
                Comment = this.Comment,
                ApprovalStatus = this.ApprovalStatus,
                UpdaterLanId = this.UpdaterLanId
            }); // anonymous params since can't pass this.  TODO: ignore props with EF NotMappedAttribute...
            CreateEmails(
                Context.GetByProc<Approver>("GetApprovalEmailVmNew", new { TaskId = Id })
            );
            return approvedBy;
        }
        public int Id { get; set; }
        public string Comment { get; set; }
        public string ApprovalStatus { get; set; }
        public string UpdaterLanId { get; set; } // used only for updates

        private void CreateEmails(List<Approver> approvers)
        {
            // *NOTE*, approvers will typically only have one row, but could have more if multiple active Delegates for a single approver position.
            var approot = HttpContext.Current.Server.MapPath("~");
            // TODO: switch on tal[0].AppName (="PdApproval")... and/or other params
            var template = System.IO.File.ReadAllText(String.Format("{0}/EmailTemplates/PdApproval.cshtml", approot));
            //set Url path
            var fullUrl = HttpContext.Current.Request.Url.ToString();
            var urlPath = fullUrl.Substring(0, fullUrl.LastIndexOf("/api/") + 1);

            foreach (var a in approvers)
            {
                a.AppBaseUrl = urlPath;
                try
                {
                    string body = Razor.Parse(template, a);
                    string subject = String.Format("Processing needed for Position Description #{0}: {1}", a.PdId, a.PositionId.HasValue ? "Position #" + a.PositionId : "No Position"); // TODO: add incumbent if not vacant
                    string fromAddress = "no-reply@cts.wa.gov";
                    string fromDisplayName = "CTS HR Apps";

                    Context.Exec("CreateEmail", parameters: new
                    {
                        fromAddress = fromAddress,
                        fromDisplayName = fromDisplayName,
                        toAddress = a.ApproverEmail,
                        toDisplayName = (a.IsHR ? "HR Office" : a.ApproverFirstName + " " + a.ApproverLastName),
                        body = body,
                        subject = subject,
                        isBodyHtml = true,
                        source = "ApprovalTask",
                        sourceId = Id
                    });

                }
                catch (Exception xcp)
                {
                    throw xcp;
                };
            }
        }
    }

    public class TaskApprover
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? CompletedDate { get; set; }
    }

    public class Approver
    {
        public int? PdId { get; set; } // Position Description Id
        public int? PositionId { get; set; } // of PD
        public string WorkingTitle { get; set; }
        public bool IsHR { get; set; }
        public string FilledByName { get; set; }
        public string TaskCodeName { get; set; }
        public string AppName { get; set; }
        public string CreatorFirstName { get; set; }
        public string CreatorLastName { get; set; }
        public int? DelegatedToPersonId { get; set; } // just for "Delegated" indicator.
        public string ApproverEmail { get; set; }
        public string ApproverFirstName { get; set; }
        public string ApproverLastName { get; set; }
        public string AppBaseUrl { get; set; }
    }

}