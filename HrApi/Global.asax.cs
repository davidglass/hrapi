﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Configuration;

namespace HrApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var app = HttpContext.Current.Application;
            GlobalConfiguration.Configure(WebApiConfig.Register);
            app["connstr"] = WebConfigurationManager.ConnectionStrings["DbConnStr"].ConnectionString;
            //Application["appname"] = WebConfigurationManager.AppSettings["AppName"];

            // per 
            // http://patrickdesjardins.com/blog/the-model-backing-the-context-has-changed-since-the-database-was-created-ef4-3
            System.Data.Entity.Database.SetInitializer<Models.HrApiContext>(null);
        }
    }
}
