﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Http.Cors;
using System.Web.Helpers; // try this if JsonResult approach fails:
//using Newtonsoft.Json;

using HrApi.Models;
using DbUtil;
using AuthFilter;

namespace HrApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials=true)]
    public class PositionDescriptionController : ApiController
    {
        // TODO: BaseApiController in Utility Lib, include hrm in that.
        private HttpResponseMessage hrm;
        // TODO: move generic DeleteById function into base class...
        //        public HttpResponseMessage Delete(int id)
        public HttpResponseMessage Delete(int id)
        {
            var pd = new PositionDescription
            {
                Id = id
            };
            try
            {
                pd.User = User;
                pd.Delete();
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError,
                    xcp.Message
                );
            }
            hrm = ControllerContext.Request.CreateResponse(HttpStatusCode.OK);
            return hrm;
        }

        // Get List
        public HttpResponseMessage Get([FromUri] PdQuery pq)
        {
            var q = pq == null ? new PdQuery() { IncludeOld = false } : pq;
            var pdq = new PdqVm
            {
                User = User,
                Query = q
            }; // TODO: pass impersonation / UserInfo here?, add GetUserInfo to BaseController...
            hrm = Request.CreateResponse<PdqVm>(pdq);
            return hrm;
        }

        // Get Detail
        //public PositionDescriptionVm Get(int id)
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse<PositionDescriptionVm>(
                new PositionDescriptionVm(User, id)
            );
        }

        // *NOTE*, PositionDescription model here was accidentally deleted.
        // It was PositionDescription model, but subclassing ModelBase
        // and with Create() performing PositionDescriptionController.Post actions.
        //public PositionDescriptionRow Post(PositionDescription pd)
        public HttpResponseMessage Post(PositionDescription pd)
        {
            HttpResponseMessage hrm;
            pd.User = User;
            try
            {
                //var pdr = pd.Create();
                // built-in Json is System.Web.Http.ApiController.Json<T>(): (JsonResult)
                //                hrm.Content = new StringContent(Json.Encode(newPd));
                //                hrm.Content = new StringContent(System.Web.Helpers.Json.Encode(newPd));
                //hrm.Content = new StringContent(Json(pdr).ToString());
                hrm = ControllerContext.Request.CreateResponse<PositionDescriptionRow>(pd.Create());
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        // TODO: try to get Attribute routing working (this wasn't, so went old-school with ActionName)
        //[Route("PositionDescription/{id}/clone")]
        //[HttpPost]
        [ActionName("Clone")]
        public HttpResponseMessage PostClone(int id)
        {
            try
            {
                var pdr = new PositionDescription { Id = id, User = User }.Clone();
                return Request.CreateResponse<PositionDescriptionRow>(pdr);
            }
            catch (Exception xcp)
            {
                // TODO: NLog
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "There was an error cloning the PDF.");
            }
        }

        public HttpResponseMessage Put(PositionDescription pd)
        {
            pd.User = User;
            try
            {
                pd.Update();
                hrm = ControllerContext.Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }
    }
}