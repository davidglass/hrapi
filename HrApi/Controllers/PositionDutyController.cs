﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Web.Helpers;
using System.Web.Http.Cors;

using HrApi.Models;
using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    //[BaseApiAuth]
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class PositionDutyController : ApiController
    {
        // TODO: move hrm back to BaseApiController...
        HttpResponseMessage hrm;
        public HttpResponseMessage Delete([FromUri] PositionDuty pd)
        {
            // TODO: move all this into HttpResponseMessage ModelBase.DeleteResult?
            try
            {
                pd.Delete();
                hrm = ControllerContext.Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        public HttpResponseMessage Post(PositionDuty pd)
        {
            try
            {
                pd.User = User;
                hrm = ControllerContext.Request.CreateResponse<PositionDuty>(pd.Create());
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                HttpStatusCode.InternalServerError,
                xcp.Message);
                // TODO: create FriendlyError (?) Model to return consistent JSON error format to client...
                //hrm.Content = new StringContent(Json.Encode(
            }
            return hrm;
        }

        public HttpResponseMessage Put(PositionDuty pd)
        {
            pd.User = User;
            try
            {
                pd.Update();
                hrm = ControllerContext.Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception xcp)
            {
                // TODO: re-enable logger...
                //logger.Error("PositionDuty update failed: " + xcp.Message);
                hrm = ControllerContext.Request.CreateErrorResponse(
                HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }
    }
}