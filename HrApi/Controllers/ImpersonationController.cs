﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Http.Cors;

using HrApi.Models;

namespace HrApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ImpersonationController : ApiController
    {
        // TODO: BaseApiController in Utility Lib, include hrm in that.
        private HttpResponseMessage hrm;

        public HttpResponseMessage Get()
        {
            try
            {
                var vm = new ImpersonationVm { User = User };
                hrm = ControllerContext.Request.CreateResponse<ImpersonationVm>(HttpStatusCode.OK, vm);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, xcp);
            }
            return hrm;
        }

        public HttpResponseMessage Put(ImpersonatedUser iu)
        {
            try
            {
                var vm = new Models.ImpersonationVm()
                {
                    User = User,
                    ImpersonatingUserId = iu.Id
                };
                vm.Update();
                var impuser = vm.GetImpersonatedUser();
                hrm = ControllerContext.Request.CreateResponse<Dictionary<string,object>>(HttpStatusCode.OK, impuser);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, xcp);
            }
            return hrm;
        }
    }

    public class ImpersonatedUser
    {
        public int? Id { get; set; }
    }
}