﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Http.Cors;
using HrApi.Models;

namespace HrApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class PositionController : ApiController
    {
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var pvm = new PositionVm(User, id);
                return Request.CreateResponse<PositionVm>(pvm);
            }
            catch (Exception xcp)
            {
                return (xcp.Message == "Access Denied.") ? Request.CreateResponse(HttpStatusCode.Forbidden, xcp.Message)
                    : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, xcp);
            }
        }
    }
}
