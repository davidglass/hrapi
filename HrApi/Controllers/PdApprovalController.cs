﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HrApi.Models;

namespace HrApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class PdApprovalController : ApiController
    {
        HttpResponseMessage hrm;
        public HttpResponseMessage Put(ApprovalTaskUpdate updateTask)
        {
            try
            {
                updateTask.User = User;
                hrm = ControllerContext.Request.CreateResponse<TaskApprover>(updateTask.Update());
                hrm.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception xcp)
            {
                //logger.Log(NLog.LogLevel.Error, xcp.Message);
                hrm = ControllerContext.Request.CreateErrorResponse(
                // InternalServer Error since Unauthorized triggers login box on IE8
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message
                );
            }
            return hrm;
        }
    }
}
