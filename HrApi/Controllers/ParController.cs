﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Http.Cors;
using System.Web.Helpers; // try this if JsonResult approach fails:
//using Newtonsoft.Json;

using HrApi.Models;
using DbUtil;
using AuthFilter;

namespace HrApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials=true)]
    public class ParController : ApiController
    {
        // TODO: BaseApiController in Utility Lib, include hrm in that.
        private HttpResponseMessage hrm;
        // Get List
        // TODO: try/catch, return HRM here...
//        public ParQueryVm Get([FromUri] ParQuery pq)
        public HttpResponseMessage Get([FromUri] ParQuery pq)
        {
            var q = pq == null ? new ParQuery() { } : pq;
            var prq = new ParQueryVm {
                User = User,
                Query = q
            };
            try
            {
                prq.Get();
                hrm = ControllerContext.Request.CreateResponse<ParQueryVm>(HttpStatusCode.OK, prq);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, xcp);
            }
            //return prq;
            return hrm;
        }

        // Get Detail
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var vm = new ParDetailVm(id, User);
                hrm = ControllerContext.Request.CreateResponse<ParDetailVm>(vm);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, xcp);
            }
            return hrm;
        }

        public HttpResponseMessage Post(ParDetail p) // switched to ParDetail to enable creating PARs from PositionDescriptions...
        {
            HttpResponseMessage hrm;
            p.User = User;
            try
            {
                hrm = ControllerContext.Request.CreateResponse<ParDetail>(p.Create());
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }
    }
}