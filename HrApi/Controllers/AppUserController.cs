﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Http.Cors;

namespace HrApi.Controllers
{
    public class AppUserController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        public Dictionary<string, object> Get()
        {
//            var u = new Models.AppUser(User).UserInfo;
            var u = new Models.AppUser { User = User }.UserInfo;
            return u;
        }
    }
}
